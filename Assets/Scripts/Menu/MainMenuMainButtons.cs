﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuMainButtons : MonoBehaviour
{
    private readonly string INSTRUCTIONS_SCENE_NAME = "3-Instructions";
    private readonly string STATISTICS_SCENE_NAME = "4-Statistics";
    private readonly string MAIN_MENU_SCENE_NAME = "1-MainMenu";

    public void GoToMainMenu()
    {
        SceneManager.LoadScene(MAIN_MENU_SCENE_NAME);
    }

    public void GoToInstructions()
    {
        SceneManager.LoadScene(INSTRUCTIONS_SCENE_NAME);
    }

    public void GoToStatistics()
    {
        SceneManager.LoadScene(STATISTICS_SCENE_NAME);
    }
}
