﻿using Assets.Scripts.Game.Models;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FreeModeContentMenu : MonoBehaviour
{
    private WorldManager worldManager;

    // Start is called before the first frame update
    public void Start()
    {
        worldManager = WorldManager.GetDontDestroyOnLoadObjects().FirstOrDefault(x => x.GetComponent<WorldManager>() != null).GetComponent<WorldManager>();
        var datasetCardResource = Resources.Load("Cards/Dataset");
        foreach (var wordDataset in worldManager.studentGameConfiguration.StudentGameConfigurationWordDatasets)
        {
            GameObject dataset = (GameObject)Instantiate(datasetCardResource, transform);
            Image[] imagesCompoents = dataset.GetComponentsInChildren<Image>();
            var imagesToDestroy = new List<GameObject>();
            for (int i = 1; i <= 4; i++)
            {
                if (i > wordDataset.WordDataset.Words.Count)
                {
                    imagesToDestroy.Add(imagesCompoents[i].gameObject);
                }
                else
                {
                    worldManager.wordImages.TryGetValue(wordDataset.WordDataset.Words[i - 1].imageUrl, out Sprite sprite);
                    imagesCompoents[i].sprite = sprite;
                }
            }
            imagesToDestroy.ForEach(x => Destroy(x));
            
            dataset.GetComponentInChildren<TMP_Text>().text = wordDataset.WordDataset.Name;
            dataset.GetComponentInChildren<TMP_Text>().alignment = TextAlignmentOptions.Center;
            dataset.GetComponentInChildren<Button>().onClick.AddListener(delegate { ConfigureGame(wordDataset.WordDataset); });
            dataset.transform.SetParent(transform);
        }
    }

    public void ConfigureGame(WordDataset wordDataset)
    {
        List<Word> availableWords = wordDataset.Words
                .Where(x => !worldManager.studentGameConfiguration.Student.StudentLearnedWords
                    .Any(y => y.id.Equals(x.id))).ToList();
        var random = new System.Random();
        int randomIndex = random.Next(0, availableWords.Count);
        worldManager.Gameplay = new Gameplay()
        {
            objective = availableWords[randomIndex],
            time = 100.0f,
            elapsedTime = 0,
            level = worldManager.GetGameConfigurationLevel(),
            score = 0,
            status = "InProgress",
            events = new List<Assets.Scripts.Tracker.Event>()
        };
        worldManager.CreateServerGameplay();
    }
    
}
