﻿using Assets.Scripts.Game.Models;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AchieventsManager : MonoBehaviour
{
    private WorldManager worldManager;
    private List<StudentAchievemet> studentAchievements;

    public void Start()
    {
        worldManager = WorldManager.GetDontDestroyOnLoadObjects().FirstOrDefault(x => x.GetComponent<WorldManager>() != null).GetComponent<WorldManager>();

        studentAchievements = worldManager.studentGameConfiguration.Student.StudentAchievements;
        studentAchievements = studentAchievements.OrderBy(sa => sa.Achievement.Order).ToList();

        var cardResource = Resources.Load("Cards/Logro");
        var lowProgress = Resources.Load<Sprite>("Sprites/LoadingBateries/row-1-col-1");
        var mediumProgress = Resources.Load<Sprite>("Sprites/LoadingBateries/row-1-col-2");
        var highProgress = Resources.Load<Sprite>("Sprites/LoadingBateries/row-1-col-3");
        var fullProgress = Resources.Load<Sprite>("Sprites/LoadingBateries/row-1-col-4");

        studentAchievements.ForEach(sa =>
        {
            GameObject achievementCard = (GameObject)Instantiate(cardResource, transform);
            achievementCard.transform.Find("Title").GetComponent<TMP_Text>().text = sa.Achievement.Name;
            achievementCard.transform.Find("Progress").GetComponent<TMP_Text>().text = sa.CurrentValue + " de " + sa.Achievement.MaxValue;
            achievementCard.transform.Find("ProgressPercentage").GetComponent<TMP_Text>().text = sa.CurrentValue * 100 / sa.Achievement.MaxValue + "%";

            if (sa.CurrentValue * 100 / sa.Achievement.MaxValue < 33.0f)
            {
                achievementCard.transform.Find("ProgressImage").GetComponent<Image>().sprite = lowProgress;
            }
            else if (sa.CurrentValue * 100 / sa.Achievement.MaxValue < 66.0f)
            {
                achievementCard.transform.Find("ProgressImage").GetComponent<Image>().sprite = mediumProgress;
            }
            else if (sa.CurrentValue * 100 / sa.Achievement.MaxValue < 100.0f)
            {
                achievementCard.transform.Find("ProgressImage").GetComponent<Image>().sprite = highProgress;
            }
            else
            {
                achievementCard.transform.Find("ProgressImage").GetComponent<Image>().sprite = fullProgress;
            }
        });
    }
}
