﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BlinkingImage : MonoBehaviour
{
    public Image image;
    public float startTime;
    // Start is called before the first frame update
    public void Start()
    {
        image.enabled = !image.enabled;
        StartBlinking();
    }

    private void StartBlinking()
    {
        StopCoroutine(Blink());
        StartCoroutine(Blink());
    }

    private IEnumerator Blink()
    {
        yield return new WaitForSeconds(startTime);
        while (true)
        {
            image.enabled = !image.enabled;
            yield return new WaitForSeconds(image.enabled ? 1.0f : 3.0f);
        }
    }
}
