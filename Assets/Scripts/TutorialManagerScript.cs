﻿using System.Collections;
using TMPro;
using UnityEngine;

public class TutorialManagerScript : MonoBehaviour
{
    public GameObject handPanel;
    public GameObject boardPanel;
    public GameObject headerPanel;
    public GameObject headerPanelScore;
    public GameObject headerPanelTime;
    public GameObject headerPanelHelpButton;
    public GameObject headerPanelObjective;
    public GameObject handPanelExitButton;
    public GameObject handPanelUndoButton;
    public GameObject handPanelGroupActionButtons;
    public GameObject groupHandAndBoradPanels;

    public GameObject transparentWindow;
    public GameObject tutorialMessagesSequence;
    public GameObject cardToAnimate;

    private Vector3 originalTransparentWindowPosition;
    private Rect originalTrasnparentWindowRect;
    private int currentPage;
    private int pageCount;
    private int[] helpMaskIndexes;

    public void Start()
    {
        this.currentPage = 1;
        this.originalTransparentWindowPosition = this.transparentWindow.transform.position;
        this.originalTrasnparentWindowRect = this.transparentWindow.GetComponent<RectTransform>().rect;

        this.pageCount = this.tutorialMessagesSequence.transform.childCount;
        SetPagesMessage(this.tutorialMessagesSequence.transform.GetChild(0));
        this.helpMaskIndexes = new int[]
        {
            7, 19, 21, 31, 35, 44, 46, 47, 58
        };
    }

    public void TransformTransparentWindow(float width, float height, Vector3 newPosition)
    {
        this.transparentWindow.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);
        this.transparentWindow.transform.position = newPosition;
        this.transparentWindow.transform.GetChild(0).transform.position = this.originalTransparentWindowPosition;
    }

    public void NextElementInSequence()
    {
        GameObject current = this.tutorialMessagesSequence.transform.GetChild(this.currentPage - 1).gameObject;
        GameObject next = this.tutorialMessagesSequence.transform.GetChild(this.currentPage).gameObject;

        this.currentPage++;

        next.SetActive(true);
        current.SetActive(false);

        SequenceAction();
        SetPagesMessage(next.transform);
    }

    public void PreviousElementInSequence()
    {
        GameObject current = this.tutorialMessagesSequence.transform.GetChild(this.currentPage - 1).gameObject;
        GameObject previous = this.tutorialMessagesSequence.transform.GetChild(this.currentPage - 2).gameObject;

        this.currentPage--;

        previous.SetActive(true);
        current.SetActive(false);

        SequenceAction();

        SetPagesMessage(previous.transform);
    }

    private void SequenceAction()
    {
        Rect rect;
        switch (this.currentPage)
        {
            case 1:
            case 2:
                TransformTransparentWindow(this.originalTrasnparentWindowRect.width, this.originalTrasnparentWindowRect.height, this.originalTransparentWindowPosition);
                break;
            case 3:
                rect = boardPanel.GetComponent<RectTransform>().rect;
                TransformTransparentWindow(rect.width, rect.height, boardPanel.transform.position);
                break;
            case 4:
                rect = handPanel.GetComponent<RectTransform>().rect;
                TransformTransparentWindow(rect.width, rect.height, handPanel.transform.position);
                break;
            case 5:
                rect = handPanelGroupActionButtons.GetComponent<RectTransform>().rect;
                TransformTransparentWindow(rect.width, rect.height, handPanelGroupActionButtons.transform.position);
                break;
            case 6:
                rect = handPanelUndoButton.GetComponent<RectTransform>().rect;
                TransformTransparentWindow(rect.width, rect.height, handPanelUndoButton.transform.position);
                break;
            case 7:
                rect = handPanelExitButton.GetComponent<RectTransform>().rect;
                TransformTransparentWindow(rect.width, rect.height, handPanelExitButton.transform.position);
                break;
            case 8:
                rect = headerPanel.GetComponent<RectTransform>().rect;
                TransformTransparentWindow(rect.width, rect.height, headerPanel.transform.position);
                break;
            case 9:
                rect = headerPanelObjective.GetComponent<RectTransform>().rect;
                TransformTransparentWindow(rect.width, rect.height, headerPanelObjective.transform.position);
                break;
            case 10:
                rect = headerPanelTime.GetComponent<RectTransform>().rect;
                TransformTransparentWindow(rect.width, rect.height, headerPanelTime.transform.position);
                break;
            case 11:
                rect = headerPanelScore.GetComponent<RectTransform>().rect;
                TransformTransparentWindow(rect.width, rect.height, headerPanelScore.transform.position);
                break;
            case 12:
                this.cardToAnimate.GetComponent<Animator>().enabled = false;
                TransformTransparentWindow(this.originalTrasnparentWindowRect.width, this.originalTrasnparentWindowRect.height, this.originalTransparentWindowPosition);
                break;
            case 13:
                rect = groupHandAndBoradPanels.GetComponent<RectTransform>().rect;
                TransformTransparentWindow(rect.width, rect.height, groupHandAndBoradPanels.transform.position);
                this.cardToAnimate.GetComponent<Animator>().enabled = true;
                break;
            case 14:
                this.cardToAnimate.GetComponent<Animator>().enabled = true;
                foreach (int i in this.helpMaskIndexes)
                {
                    boardPanel.transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
                }
                break;
            case 15:
                rect = headerPanelHelpButton.GetComponent<RectTransform>().rect;
                TransformTransparentWindow(rect.width, rect.height, headerPanelHelpButton.transform.position);
                StartCoroutine(StopAnimation());
                break;
            case 16:
                TransformTransparentWindow(this.originalTrasnparentWindowRect.width, this.originalTrasnparentWindowRect.height, this.originalTransparentWindowPosition);
                break;
        }
    }

    private void SetPagesMessage(Transform message)
    {
        var pagesText = message
                    .Find("Buttons")
                    .Find("Pages")
                    .GetComponent<TMP_Text>();

        pagesText.text = pagesText.text
            .Replace("{currentPage}", this.currentPage.ToString())
            .Replace("{pageCount}", this.pageCount.ToString());
    }

    private IEnumerator StopAnimation()
    {
        this.cardToAnimate.GetComponent<Animator>().ForceStateNormalizedTime(0);
        yield return new WaitForEndOfFrame();
        this.cardToAnimate.GetComponent<Animator>().enabled = false;
    }
}
