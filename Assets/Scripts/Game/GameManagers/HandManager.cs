﻿using Assets.Scripts.Game.Models;
using Assets.Scripts.Models;
using Assets.Scripts.Tracker;
using Assets.Scripts.Tracker.EventAttributes;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HandManager : MonoBehaviour
{
    public GameObject gameManagerObject;

    public string[] targetWords;

    private int numGeneratedCharacters;

    private List<List<GameObject>> hand;

    private const int NUM_HAND_ROWS = 5;
    private const int NUM_HAND_COLS = 2;
    private readonly int MAX_CHARACTERS = TableManager.MAX_NUM_COLS * TableManager.MAX_NUM_ROWS;

    private int cardsInHand;
    private List<char> ALLOWED_CHARACTERS;
    // Start is called before the first frame update
    public void Start()
    {
        numGeneratedCharacters = 0;
        hand = new List<List<GameObject>>();
        /*this.ALLOWED_CHARACTERS = new List<char>() {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
            'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q',
            'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        };*/
        //targetWords = new string[] { "pera", "manzana", "naranja" };
        
    }

    public void BuildAllowedCharacters()
    {
        this.ALLOWED_CHARACTERS = new List<char>();
        foreach (string word in targetWords)
        {
            foreach (char character in word)
            {
                this.ALLOWED_CHARACTERS.Add(character);
            }
        }
    }

    public void GenerateHand(){
        System.Random rnd = new System.Random();
        var cardResource = Resources.Load("Cards/CardA");
        Vector3 lossyScale = gameObject.transform.lossyScale;
        for (int i = 0; i < NUM_HAND_ROWS; i++)
        {
            hand.Add(new List<GameObject>());
            for (int j = 0; j < NUM_HAND_COLS; j++)
            {
                int referenceTileIndex  = rnd.Next(0, ALLOWED_CHARACTERS.Count);

                Sprite randomCharacterSprite = Resources.Load<Sprite>("Sprites/characters/upper/" + ALLOWED_CHARACTERS[referenceTileIndex].ToString().ToUpper());

                GameObject tile = new GameObject("HandTile" + (i * NUM_HAND_COLS + j).ToString(), typeof(RectTransform))
                {
                    layer = (int)LayerEnum.UI
                };
                tile.transform.localScale = new Vector3(lossyScale.x, lossyScale.y, lossyScale.z);
                tile.transform.SetParent(transform);

                GameObject characterCard = (GameObject)Instantiate(cardResource, tile.transform);
                characterCard.transform.GetChild(0).GetComponent<Image>().sprite = randomCharacterSprite;
                characterCard.GetComponent<DragAndDrop>().gameManagerObject = gameManagerObject.GetComponent<GameManagerScript>();
                characterCard.GetComponent<BoardTile>().Name = "CharacterCard";
                characterCard.GetComponent<BoardTile>().Value = ALLOWED_CHARACTERS[referenceTileIndex];
                characterCard.GetComponent<BoardTile>().IsDraggable = true;

                hand[i].Add(tile);

                numGeneratedCharacters++;
                cardsInHand++;
            }
        }

        GameObject tileUndoButton = new GameObject("HandHelpButton", typeof(RectTransform));
        tileUndoButton.transform.localScale = new Vector3(lossyScale.x, lossyScale.y, lossyScale.z);
        tileUndoButton.transform.SetParent(transform);

        GameObject undoButton = (GameObject)Instantiate(Resources.Load("Cards/UndoButton"), tileUndoButton.transform);

        GameObject tileExitButton = new GameObject("HandExitButton", typeof(RectTransform));
        tileExitButton.transform.localScale = new Vector3(lossyScale.x, lossyScale.y, lossyScale.z);
        tileExitButton.transform.SetParent(transform);

        GameObject exitButton = (GameObject)Instantiate(Resources.Load("Cards/ExitButton"), tileExitButton.transform);
        exitButton.GetComponentInChildren<Button>().onClick.AddListener(gameManagerObject.GetComponent<GameManagerScript>().AbandoneGame());
    }

    public bool IsValidMovement(GameObject targetCard, GameObject characterCard)
    {
        if (IsTargetCardInHand(targetCard, characterCard))
        {
            return false;
        }
        return true;
    }

    public void ManageCardsInteraction(CardsInteractionModel cardsInteractionModel)
    {
        var hasToGenerateMoreCharacters = HasToGenerateMoreCharacters();

        for (int i = 0; i < NUM_HAND_ROWS; i++)
        {
            for (int j = 0; j < NUM_HAND_COLS; j++)
            {
                if (hand[i][j].GetHashCode() == cardsInteractionModel.CharacterCard.GetHashCode())
                {
                    cardsInteractionModel.OriginRow = i;
                    cardsInteractionModel.OriginCol = j;
                    if (hasToGenerateMoreCharacters)
                    {
                        GameObject card = GenerateNextCharacter();
                        cardsInteractionModel.GeneratedCard = card;

                        var auxParent = hand[i][j].transform.GetChild(0).parent;
                        hand[i][j].transform.GetChild(0).position = cardsInteractionModel.TargetEmptyCard.transform.GetChild(0).position;
                        hand[i][j].transform.GetChild(0).SetParent(cardsInteractionModel.TargetEmptyCard.transform.GetChild(0).parent);
                        card.transform.SetParent(auxParent);
                        card.GetComponent<RectTransform>().anchoredPosition = cardsInteractionModel.OriginalCharacterCardPosition;
                        Destroy(cardsInteractionModel.TargetEmptyCard.transform.GetChild(0).gameObject);
                    }
                    else
                    {
                        cardsInHand--;
                        cardsInteractionModel.GeneratedCard = null;
                        var auxParent = hand[i][j].transform.GetChild(0).parent;
                        hand[i][j].transform.GetChild(0).position = cardsInteractionModel.TargetEmptyCard.transform.GetChild(0).position;
                        hand[i][j].transform.GetChild(0).SetParent(cardsInteractionModel.TargetEmptyCard.transform.GetChild(0).parent);
                        cardsInteractionModel.TargetEmptyCard.transform.GetChild(0).SetParent(auxParent);
                        auxParent.GetChild(0).GetComponent<RectTransform>().anchoredPosition = cardsInteractionModel.OriginalCharacterCardPosition;
                    }
                }
            }
        }
    }

    public void EndOfGame()
    {
        SetAllCardsAsNotDraggable();
    }

    public bool IsTargetCardInHand(GameObject targetCard, GameObject characterCard)
    {
        for (int i = 0; i < NUM_HAND_ROWS; i++)
        {
            for (int j = 0; j < NUM_HAND_COLS; j++)
            {
                if (hand[i][j].GetHashCode() == targetCard.GetHashCode())
                {
                    this.gameManagerObject.GetComponent<GameManagerScript>().TrackEvent(new Assets.Scripts.Tracker.Event()
                    {
                        verbAttribute = new VerbEventAttribute() { id = "http://adlnet.gov/expapi/verbs/interacted" },
                        objectAttribute = new ObjectEventAttribute()
                        {
                            id = "https://crazy-words.e-ucm.es/xapi/character/" + characterCard.GetComponentInChildren<BoardTile>().Value ?? "none",
                            definition = new ObjectDefinitionEventAttribute() { type = "https://crazy-words.e-ucm.es/xapi/seriousgames/interaction/move" }
                        },
                        resultAttribute = new Dictionary<string, object>
                        {
                            { "success" , false },
                            { "extensions", new Dictionary<string, object>
                                {
                                    { "https://crazy-words.e-ucm.es/xapi/game/invalid-movement-reason", "Destination is in hand" },
                                    { "https://crazy-words.e-ucm.es/xapi/table/DestinationRow", i },
                                    { "https://crazy-words.e-ucm.es/xapi/table/DestinationCol", j }
                                }
                            }
                        }
                    });
                    return true;
                }
            }
        }
        return false;
    }

    public List<List<char>> GetHandCharactersDistribution()
    {
        return hand.Select(x => x.Select(y => y.transform.GetChild(0).GetComponent<BoardTile>().Value ?? ' ').ToList()).ToList();
    }

    public bool ThereIsMoreCardsInHand()
    {
        return this.cardsInHand == 0;
    }

    private bool HasToGenerateMoreCharacters()
    {
        return MAX_CHARACTERS > numGeneratedCharacters;
    }

    private GameObject GenerateNextCharacter()
    {
        var cardResource = Resources.Load("Cards/CardA");

        System.Random rnd = new System.Random();
        int referenceTileIndex = rnd.Next(0, ALLOWED_CHARACTERS.Count);

        Sprite randomCharacterSprite = Resources.Load<Sprite>("Sprites/characters/upper/" + ALLOWED_CHARACTERS[referenceTileIndex].ToString().ToUpper());

        GameObject characterCard = (GameObject)Instantiate(cardResource);

        Vector3 lossyScale = gameObject.transform.lossyScale;
        characterCard.transform.localScale = new Vector3(lossyScale.x, lossyScale.y, lossyScale.z);

        characterCard.transform.GetChild(0).GetComponent<Image>().sprite = randomCharacterSprite;
        characterCard.GetComponent<DragAndDrop>().gameManagerObject = gameManagerObject.GetComponent<GameManagerScript>();
        characterCard.GetComponent<BoardTile>().Name = "CharacterCard";
        characterCard.GetComponent<BoardTile>().Value = ALLOWED_CHARACTERS[referenceTileIndex];
        characterCard.GetComponent<BoardTile>().IsDraggable = true;

        numGeneratedCharacters++;

        return characterCard;
    }

    private void SetAllCardsAsNotDraggable()
    {
        hand.ForEach(row =>
            row.ForEach(card =>
                card.GetComponentInChildren<BoardTile>().IsDraggable = false
            )
        );
    }
}
