﻿using Assets.Scripts.Tracker.EventAttributes;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HeaderManager : MonoBehaviour
{
    public GameObject TimeObject;
    public GameObject ScoreObject;
    public GameObject ObjectiveObject;
    public GameObject HelpObject;
    public GameObject TimerCountdownObject;

    public GameObject ObjectiveHelpMessage;

    public GameObject gameManagerObject;

    public string[] targetWords;

    private int gameLevelSelected;

    public void SetObjective(Sprite objective)
    {
        var image = ObjectiveObject.GetComponentInChildren<Image>();
        image.sprite = objective;
    }

    public void SetMaximumTime(float time)
    {
        TimeObject.GetComponentInChildren<ProgressBarTimer>().maxTime = time;
    }

    public void SetLeftTime(float time)
    {
        TimeObject.GetComponentInChildren<ProgressBarTimer>().leftTime = time;
    }

    public float GetLeftTime()
    {
        return TimeObject.GetComponentInChildren<ProgressBarTimer>().leftTime;
    }

    public void SetScore(int score)
    {
        ScoreObject.GetComponentInChildren<TMP_Text>().text = "Puntuación: " + score;
    }

    public void TimesUp()
    {
        gameManagerObject.GetComponent<GameManagerScript>().TimesUp();
    }

    public bool IsGameFinished()
    {
        return gameManagerObject.GetComponent<GameManagerScript>().IsGameFinished();
    }

    public void ManageIdlePlayer()
    {
        gameManagerObject.GetComponent<GameManagerScript>().ManageIdlePlayer();
        StopTimer();
    }

    public void StopTimer()
    {
        TimeObject.GetComponentInChildren<ProgressBarTimer>().StopTimer();
    }

    public void ManageDialogClosed()
    {
        TimeObject.GetComponentInChildren<ProgressBarTimer>().ResumeTimer();
        TimeObject.GetComponentInChildren<ProgressBarTimer>().ActionDetected();
    }

    public void ActionDetected()
    {
        TimeObject.GetComponentInChildren<ProgressBarTimer>().ActionDetected();
    }

    public float GetElapsedtime()
    {
        var timer = TimeObject.GetComponentInChildren<ProgressBarTimer>();
        return timer.maxTime - timer.leftTime;
    }

    public void StartGame()
    {
        this.TimerCountdownObject.SetActive(true);
        this.ScoreObject.SetActive(true);
        this.ObjectiveObject.SetActive(true);
    }

    public void NotifyGameLevel(int level)
    {
        this.gameLevelSelected = level;
        switch (this.gameLevelSelected)
        {
            case 0: ActivateEasyLevel(); break;
            case 1: ActivateMediumLevel(); break;
            case 2: ActivateHardLevel(); break;
            default: break;
        }
    }
    
    public void ShowObjectiveHelp()
    {
        this.ObjectiveHelpMessage.SetActive(true);
        TimeObject.GetComponentInChildren<ProgressBarTimer>().ActionDetected();
        StopTimer();
        this.gameManagerObject.GetComponent<GameManagerScript>().TrackEvent(new Assets.Scripts.Tracker.Event()
        {
            verbAttribute = new VerbEventAttribute() { id = "http://adlnet.gov/expapi/verbs/interacted" },
            objectAttribute = new ObjectEventAttribute()
            {
                id = "https://crazy-words.e-ucm.es/xapi/help/objective",
                definition = new ObjectDefinitionEventAttribute() { type = "https://w3id.org/xapi/seriousgames/activity-types/item" }
            },
        });
    }

    public void HelpButtonClicked()
    {
        this.gameManagerObject.GetComponent<GameManagerScript>().RemarkValidPositions();
        this.gameManagerObject.GetComponent<GameManagerScript>().TrackEvent(new Assets.Scripts.Tracker.Event()
        {
            verbAttribute = new VerbEventAttribute() { id = "http://adlnet.gov/expapi/verbs/interacted" },
            objectAttribute = new ObjectEventAttribute()
            {
                id = "https://crazy-words.e-ucm.es/xapi/help/button",
                definition = new ObjectDefinitionEventAttribute() { type = "https://w3id.org/xapi/seriousgames/activity-types/item" }
            },
        });
    }

    private void ActivateHardLevel()
    {
        this.ObjectiveObject.GetComponentInChildren<Button>().enabled = false;
        this.HelpObject.SetActive(false);
        this.gameManagerObject.GetComponent<GameManagerScript>().SetIsRemarkAlwaysValidPosition(false);
    }

    private void ActivateMediumLevel()
    {
        Transform textGroup = this.ObjectiveHelpMessage.transform.Find("TextWithWordLength");
        textGroup.gameObject.SetActive(true);
        TMP_Text textComponent = textGroup.Find("Text").GetComponent<TMP_Text>();
        textComponent.text = textComponent.text.Replace("{NumCharacters}", targetWords[0].Length.ToString());
        TMP_Text lengthComponent = textGroup.Find("Length").GetComponent<TMP_Text>();

        var wordLengthString = new StringBuilder();
        for(int i = 0; i< targetWords[0].Length; i++)
        {
            wordLengthString.Append('_');
        }
        lengthComponent.text = wordLengthString.ToString();

        this.HelpObject.SetActive(true);
        this.gameManagerObject.GetComponent<GameManagerScript>().SetIsRemarkAlwaysValidPosition(false);
    }

    private void ActivateEasyLevel()
    {
        this.ObjectiveHelpMessage.transform.Find("TextWithWord").gameObject.SetActive(true);

        Transform textGroup = this.ObjectiveHelpMessage.transform.Find("TextWithWord");
        textGroup.gameObject.SetActive(true);
        TMP_Text textComponent = textGroup.Find("Word").GetComponent<TMP_Text>();
        textComponent.text = targetWords[0].ToUpper();

        this.HelpObject.SetActive(false);
        this.gameManagerObject.GetComponent<GameManagerScript>().SetIsRemarkAlwaysValidPosition(true);
    }
}
