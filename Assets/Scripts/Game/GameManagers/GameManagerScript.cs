﻿using Assets.Scripts.Game.Models;
using Assets.Scripts.Models;
using Assets.Scripts.Tracker.EventAttributes;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour
{
    public HandManager handManagerObject;
    public TableManager tableManagerObject;
    public HeaderManager headerManagerObject;
    public Image initialMessageObjectiveImage;
    public GameObject initalMessage;
    public GameObject suggestUpgradeLevelMessage;
    public GameObject suggestDowngradeLevelMessage;
    public GameObject winMessage;
    public GameObject loseMessage;
    public GameObject finalScoreMessage;
    public GameObject idleMessage;
    public GameObject consecutiveInvalidMovementsMessage;

    private WorldManager worldManager;
    private Gameplay gameplay;
    private bool isGameFinished;
    private int consecutiveInvalidMovements;
    private bool isRemarkValidPositionsAlwaysEnable;

    public void Start()
    {
        this.isGameFinished = false;
        this.consecutiveInvalidMovements = 0;
        worldManager = WorldManager.GetDontDestroyOnLoadObjects().FirstOrDefault(x => x.GetComponent<WorldManager>() != null).GetComponent<WorldManager>();

        gameplay = worldManager.Gameplay;
        headerManagerObject.targetWords = new string[] { gameplay.objective.value };
        worldManager.wordImages.TryGetValue(gameplay.objective.imageUrl, out Sprite objectiveSprite);
        headerManagerObject.SetObjective(objectiveSprite);
        headerManagerObject.SetMaximumTime(gameplay.time);
        headerManagerObject.SetLeftTime(gameplay.time);
        headerManagerObject.SetScore(gameplay.score);

        winMessage.GetComponentsInChildren<Image>().First(x => x.name == "ObjectiveImage").sprite = objectiveSprite;
        winMessage.GetComponentsInChildren<TMP_Text>().First(x => x.name == "ObjectiveText").text = gameplay.objective.value.ToLower();

        initialMessageObjectiveImage.sprite = objectiveSprite;

        ManageSuggestChangeLevel();
    }

    public void Update()
    {
        this.worldManager.Gameplay.elapsedTime = (int)this.headerManagerObject.GetElapsedtime();
    }

    public void ActionDetected()
    {
        this.headerManagerObject.ActionDetected();
    }

    public void ManageIdlePlayer()
    {
        this.idleMessage.SetActive(true);
        this.TrackEvent(new Assets.Scripts.Tracker.Event()
        {
            verbAttribute = new VerbEventAttribute() { id = "http://adlnet.gov/expapi/verbs/idle" }
        });
    }

    public void ManageReturnFromIdle()
    {
        this.idleMessage.SetActive(false);
        this.headerManagerObject.ManageDialogClosed();
        this.TrackEvent(new Assets.Scripts.Tracker.Event()
        {
            verbAttribute = new VerbEventAttribute() { id = "http://adlnet.gov/expapi/verbs/return-from-idle" }
        });
    }

    public void ManageInvalidMovement()
    {
        if (++this.consecutiveInvalidMovements >= 5)
        {
            this.headerManagerObject.StopTimer();
            this.consecutiveInvalidMovementsMessage.SetActive(true);
        }
    }

    public void ManageCloseConsecutiveInvalidMovementsDialog()
    {
        this.consecutiveInvalidMovementsMessage.SetActive(false);
        this.headerManagerObject.ManageDialogClosed();
    }

    public bool IsValidMovement(CardsInteractionModel cardsInteractionModel)
    {
        return handManagerObject.IsValidMovement(cardsInteractionModel.TargetEmptyCard, cardsInteractionModel.CharacterCard)
            && (tableManagerObject.IsValidMovement(cardsInteractionModel.TargetEmptyCard, cardsInteractionModel.CharacterCard));
    }

    public void ManageCardsInteraction(CardsInteractionModel cardsInteractionModel)
    {
        this.consecutiveInvalidMovements = 0;

        handManagerObject.ManageCardsInteraction(cardsInteractionModel);
        tableManagerObject.ManageCardsInteraction(cardsInteractionModel);

        int movementScore = 5;
        AddScore(movementScore);

        StartCoroutine(UpdateHelpMarks());

        TrackSuccessfullyCompletedMoveEvent(cardsInteractionModel, movementScore);
        TrackGeneratedCardEvent(cardsInteractionModel);

        CheckEndOfGame();
    }

    private IEnumerator UpdateHelpMarks()
    {
        yield return new WaitForEndOfFrame();
        this.tableManagerObject.GetComponent<TableManager>().RemoveAllValidPositionMarks();
        if (isRemarkValidPositionsAlwaysEnable) RemarkValidPositions();
    }

    public string GetTargetWord()
    {
        return this.worldManager.Gameplay.objective.value;
    }

    public bool IsGameFinished()
    {
        return this.isGameFinished;
    }

    public void AddScore(int score)
    {
        this.worldManager.Gameplay.score += score;
        this.headerManagerObject.SetScore(this.worldManager.Gameplay.score);
    }

    public void CheckEndOfGame()
    {
        if (this.tableManagerObject.isTargetWordAchived)
        {
            this.isGameFinished = true;
            this.worldManager.Gameplay.status = "Win";
            this.worldManager.Gameplay.elapsedTime = (int)headerManagerObject.GetElapsedtime();
            int leftTimeScore = this.worldManager.Gameplay.elapsedTime * 5;
            this.AddScore(leftTimeScore);
            this.headerManagerObject.SetScore(this.worldManager.Gameplay.score);
            TrackEndEvent("Mission accomplish", true);
            handManagerObject.EndOfGame();
            ShowWinMessage();
        }
        else if (handManagerObject.ThereIsMoreCardsInHand())
        {
            this.isGameFinished = true;
            this.worldManager.Gameplay.status = "Lose";
            this.worldManager.Gameplay.elapsedTime = (int)headerManagerObject.GetElapsedtime();
            TrackEndEvent("Mision faild. No more cards.", false);
            handManagerObject.EndOfGame();
            ShowLoseMessage();
        }
    }

    public void TrackEvent(Assets.Scripts.Tracker.Event eventToTrack)
    {
        this.worldManager.TrackGameEvent(eventToTrack);
    }

    public void TimesUp()
    {
        this.isGameFinished = true;
        this.worldManager.Gameplay.status = "Lose";
        this.worldManager.Gameplay.elapsedTime = (int)headerManagerObject.GetElapsedtime();
        TrackEndEvent("Mision failed. Time's up.", false);
        handManagerObject.EndOfGame();
        ShowLoseMessage();
    }

    public bool CanDrag()
    {
        return !this.idleMessage.activeSelf;
    }

    public void ConfirmChangeLevel()
    {
        var suggestChangeLevelValue = this.worldManager.GetSuggestChangeLevelValue();
        if (this.worldManager.Gameplay.level == 0 && suggestChangeLevelValue >= 5)
        {
            this.worldManager.Gameplay.level++;
        }
        else if (this.worldManager.Gameplay.level == 1 && suggestChangeLevelValue >= 10)
        {
            this.worldManager.Gameplay.level++;
        }
        else if (this.worldManager.Gameplay.level == 2 && suggestChangeLevelValue <= -10)
        {
            this.worldManager.Gameplay.level--;
        }
        else if (this.worldManager.Gameplay.level == 1 && suggestChangeLevelValue <= -5)
        {
            this.worldManager.Gameplay.level--;
        }
        this.suggestUpgradeLevelMessage.SetActive(false);
        this.suggestDowngradeLevelMessage.SetActive(false);
        this.initalMessage.SetActive(true);
    }

    public void CancelChangeLevel()
    {
        this.suggestUpgradeLevelMessage.SetActive(false);
        this.suggestDowngradeLevelMessage.SetActive(false);
        this.initalMessage.SetActive(true);
    }

    public void InitialDialogClosed()
    {
        this.headerManagerObject.NotifyGameLevel(this.worldManager.Gameplay.level);
        this.headerManagerObject.StartGame();
        this.LoadHandPanel();
    }

    public void RemarkValidPositions()
    {
        this.tableManagerObject.GetComponent<TableManager>().RemarkValidPositions();
    }

    public void SetIsRemarkAlwaysValidPosition(bool value)
    {
        this.isRemarkValidPositionsAlwaysEnable = value;
    }

    public UnityAction AbandoneGame()
    {
        return () =>
        {
            this.isGameFinished = true;
            this.worldManager.Gameplay.status = "Lose";
            this.worldManager.Gameplay.elapsedTime = (int)headerManagerObject.GetElapsedtime();
            TrackEndEvent("Abandone.", false);
            handManagerObject.EndOfGame();
            ShowLoseMessage();
        };
    }

    private void ShowWinMessage()
    {
        this.winMessage.SetActive(true);
        SetScoreTexts();
    }

    private void ShowLoseMessage()
    {
        this.loseMessage.SetActive(true);
        SetScoreTexts();
    }

    private void SetScoreTexts()
    {
        var currentScoreObject = this.finalScoreMessage.transform.Find("CurrentScoreText");
        currentScoreObject.GetComponent<TMP_Text>().text = currentScoreObject.GetComponent<TMP_Text>().text.Replace("{currentScore}", this.gameplay.score.ToString());

        var highScore = Mathf.Max(this.gameplay.score, this.worldManager.studentGameConfiguration.HighScore);

        var highScoreObject = this.finalScoreMessage.transform.Find("HighScoreText");
        highScoreObject.GetComponent<TMP_Text>().text = highScoreObject.GetComponent<TMP_Text>().text.Replace("{highScore}", highScore.ToString());

        var newHighScoreObject = this.finalScoreMessage.transform.Find("NewHighScoreText");
        newHighScoreObject.gameObject.SetActive(this.worldManager.studentGameConfiguration.HighScore != highScore);
    }

    private void TrackEndEvent(string reason, bool success)
    {
        List<List<char>> handInitialCharactersDistribution = handManagerObject.GetHandCharactersDistribution();
        List<List<char>> tableInitialCharactersDistibution = tableManagerObject.GetTableCharactersDistribution();

        TrackEvent(new Assets.Scripts.Tracker.Event()
        {
            verbAttribute = new VerbEventAttribute() { id = "http://adlnet.gov/expapi/verbs/completed" },
            objectAttribute = new ObjectEventAttribute()
            {
                id = "https://crazy-words.e-ucm.es/xapi/word/" + gameplay.objective.id,
                definition = new ObjectDefinitionEventAttribute() { type = "https://w3id.org/xapi/seriousgames/activity-types/screen" },

            },
            resultAttribute = new Dictionary<string, object>
            {
                { "score", new ResultScoreEventAttribute()
                    {
                        scaledAttribute = gameplay.score / (390.0f + 5.0f * gameplay.time),
                        rawAttribute = gameplay.score,
                        minAttribute = 0.0f,
                        maxAttribute = 390.0f + 5.0f * gameplay.time
                    }
                },
                { "success" , success },
                { "completion", true },
                { "extensions", new Dictionary<string, object>
                    {
                        { "https://w3id.org/xapi/seriousgames/extensions/progress", 1 },
                        { "https://crazy-words.e-ucm.es/xapi/hand", handInitialCharactersDistribution },
                        { "https://crazy-words.e-ucm.es/xapi/table", tableInitialCharactersDistibution },
                        { "https://crazy-words.e-ucm.es/xapi/game/finish-reason", reason },
                        { "https://crazy-words.e-ucm.es/xapi/game/left-time", "P" + (int)this.headerManagerObject.GetElapsedtime() + "S" }
                    }
                }
            }
        });
    }

    private void TrackGeneratedCardEvent(CardsInteractionModel cardsInteractionModel)
    {
        if (cardsInteractionModel.GeneratedCard != null)
        {
            TrackEvent(new Assets.Scripts.Tracker.Event()
            {
                verbAttribute = new VerbEventAttribute() { id = "http://adlnet.gov/expapi/verbs/generate-card" },
                objectAttribute = new ObjectEventAttribute()
                {
                    id = "https://crazy-words.e-ucm.es/xapi/character/" + cardsInteractionModel.GeneratedCard.GetComponent<BoardTile>().Value ?? "none",
                    definition = new ObjectDefinitionEventAttribute() { type = "https://w3id.org/xapi/seriousgames/activity-types/screen" },

                },
                resultAttribute = new Dictionary<string, object>
                {
                    { "extensions", new Dictionary<string, object>
                        {
                            { "https://crazy-words.e-ucm.es/xapi/hand/OriginRow", cardsInteractionModel.OriginRow },
                            { "https://crazy-words.e-ucm.es/xapi/hand/OriginCol", cardsInteractionModel.OriginCol }
                        }
                    }
                }
            });
        }
    }

    private void TrackSuccessfullyCompletedMoveEvent(CardsInteractionModel cardsInteractionModel, int movementScore)
    {
        TrackEvent(new Assets.Scripts.Tracker.Event()
        {
            verbAttribute = new VerbEventAttribute() { id = "http://adlnet.gov/expapi/verbs/interacted" },
            objectAttribute = new ObjectEventAttribute()
            {
                id = "https://crazy-words.e-ucm.es/xapi/word/" + gameplay.objective.id,
                definition = new ObjectDefinitionEventAttribute() { type = "https://w3id.org/xapi/seriousgames/activity-types/screen" },

            },
            resultAttribute = new Dictionary<string, object>
            {
                { "success" , true },
                { "completion", false },
                { "extensions", new Dictionary<string, object>
                    {
                        { "https://crazy-words.e-ucm.es/xapi/hand/OriginRow", cardsInteractionModel.OriginRow },
                        { "https://crazy-words.e-ucm.es/xapi/hand/OriginCol", cardsInteractionModel.OriginCol },
                        { "https://crazy-words.e-ucm.es/xapi/table/DestinationRow", cardsInteractionModel.DestinationRow },
                        { "https://crazy-words.e-ucm.es/xapi/table/DestinationCol", cardsInteractionModel.DestinationCol },
                        { "https://crazy-words.e-ucm.es/xapi/score/add", movementScore }
                    }
                }
            }
        });
    }

    private void ManageSuggestChangeLevel()
    {
        var suggestChangeLevelValue = this.worldManager.GetSuggestChangeLevelValue();
        if ((this.worldManager.Gameplay.level == 0 && suggestChangeLevelValue >= 5) ||
            (this.worldManager.Gameplay.level == 1 && suggestChangeLevelValue >= 10))
        {
            this.initalMessage.SetActive(false);
            this.suggestUpgradeLevelMessage.SetActive(true);
        }
        else if ((this.worldManager.Gameplay.level == 2 && suggestChangeLevelValue <= -10) ||
            (this.worldManager.Gameplay.level == 1 && suggestChangeLevelValue <= -5))
        {
            this.initalMessage.SetActive(false);
            this.suggestDowngradeLevelMessage.SetActive(true);
        }
    }

    private void LoadHandPanel()
    {
        handManagerObject.targetWords = new string[] { gameplay.objective.value };
        handManagerObject.BuildAllowedCharacters();
        handManagerObject.GenerateHand();
        List<List<char>> handInitialCharactersDistribution = handManagerObject.GetHandCharactersDistribution();
        List<List<char>> tableInitialCharactersDistibution = tableManagerObject.GetTableCharactersDistribution();

        TrackEvent(new Assets.Scripts.Tracker.Event()
        {
            verbAttribute = new VerbEventAttribute() { id = "http://adlnet.gov/expapi/verbs/initialize" },
            objectAttribute = new ObjectEventAttribute()
            {
                id = "https://crazy-words.e-ucm.es/xapi/word/" + gameplay.objective.id,
                definition = new ObjectDefinitionEventAttribute() { type = "https://w3id.org/xapi/seriousgames/activity-types/screen" },

            },
            resultAttribute = new Dictionary<string, object>
            {
                { "score", new ResultScoreEventAttribute()
                    {
                        scaledAttribute = 0.0f,
                        rawAttribute = 0.0f,
                        minAttribute = 0.0f,
                        maxAttribute = 390.0f + 5.0f * gameplay.time
                    }
                },
                { "completion", false },
                { "extensions", new Dictionary<string, object>
                    {
                        { "https://w3id.org/xapi/seriousgames/extensions/progress",  0 },
                        { "https://crazy-words.e-ucm.es/xapi/hand", handInitialCharactersDistribution },
                        { "https://crazy-words.e-ucm.es/xapi/table", tableInitialCharactersDistibution }
                    }
                }
            }
        });
    }
}
