﻿using System.Linq;
using UnityEngine;

public class EndGameMessagesScript : MonoBehaviour
{
    //private readonly string MAIN_MENU_SCENE_NAME = "1-MainMenu";

    public void GoToMainMenu()
    {
        var worldManager = WorldManager.GetDontDestroyOnLoadObjects().FirstOrDefault(x => x.GetComponent<WorldManager>() != null).GetComponent<WorldManager>();
        StartCoroutine(worldManager.EndServerGameplay());
    }
}
