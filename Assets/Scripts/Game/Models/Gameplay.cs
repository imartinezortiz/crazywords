﻿using Assets.Scripts.Game.Models;
using Assets.Scripts.Tracker;
using System;
using System.Collections.Generic;

[Serializable]
public class Gameplay
{
    public string id;
    public Word objective;
    public float time;
    public int elapsedTime;
    public int level;
    public int score;
    public string status;
    public List<Event> events;
    public DateTimeOffset startDate;
    public DateTimeOffset endDate;
}
