﻿using System.Collections.Generic;

namespace Assets.Scripts.Game.Models
{
    public class StudentGameConfiguration 
    {
        public Student Student { get; set; }
        public int Level { get; set; }
        public int SuggestChangeLevelValue { get; set; }
        public int HighScore { get; set; }
        public List<StudentGameConfigurationWordDataset> StudentGameConfigurationWordDatasets { get; set; }
    }
}
