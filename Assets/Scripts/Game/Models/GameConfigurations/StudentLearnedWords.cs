﻿
using System.Collections.Generic;

namespace Assets.Scripts.Game.Models
{
    public class StudentLearnedWords 
    {
        public List<Word> Words { get; set; }
    }
}
