﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Game.Models
{
    public class WordDataset 
    {
        public Guid? TeacherId { get; set; }

        public string Name { get; set; }

        public bool IsPublic { get; set; }

        public List<Word> Words { get; set; }
    }
}
