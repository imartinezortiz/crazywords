﻿using UnityEngine;

namespace Assets.Scripts.Game.Models
{
    public class BoardTile: MonoBehaviour
    {
        public bool IsDraggable;
        public string Name;
        public char? Value;
        public GameObject helpMark;
    }
}
