﻿public class CheckIdleScript
{
    private float lastActionTime;
    private float currentTime;
    private const float MAX_ALLOWED_IDLE_TIME = 10.0f;

    public void RestartTime(float currentTime)
    {
        this.lastActionTime = currentTime;
        this.currentTime = currentTime;
    }

    public void UpdateTime(float currentTime)
    {
        this.currentTime = currentTime;
    }

    public bool IsPlayerIdle()
    {
        return this.currentTime - this.lastActionTime >= MAX_ALLOWED_IDLE_TIME;
    }

}
