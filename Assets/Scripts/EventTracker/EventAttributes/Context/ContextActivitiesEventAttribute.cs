﻿namespace Assets.Scripts.Tracker.EventAttributes
{
    public class ContextActivitiesEventAttribute
    {
        public ContextActivitiesItemEventAttribute[] parent;
        public ContextActivitiesItemEventAttribute[] grouping;
        public ContextActivitiesItemEventAttribute[] category = new ContextActivitiesItemEventAttribute[]
        {
            new ContextActivitiesItemEventAttribute(){ id = "https://w3id.org/xapi/seriousgames/v1.0" }
        };
        public ContextActivitiesItemEventAttribute[] other;
    }
}