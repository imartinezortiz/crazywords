﻿using Newtonsoft.Json;

namespace Assets.Scripts.Tracker.EventAttributes
{
    public class ResultScoreEventAttribute
    {
        [JsonProperty(PropertyName = "scaled")]
        public float scaledAttribute;

        [JsonProperty(PropertyName = "raw")]
        public float rawAttribute;

        [JsonProperty(PropertyName = "min")]
        public float minAttribute;

        [JsonProperty(PropertyName = "max")]
        public float maxAttribute;
    }
}