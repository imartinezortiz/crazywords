﻿namespace Assets.Scripts.Tracker.EventAttributes
{
    public class ActorEventAttribute
    {
        public string name;
        public ActorAccountEventAttribute account;
    }
}
