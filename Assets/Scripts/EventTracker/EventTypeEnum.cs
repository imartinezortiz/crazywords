﻿namespace Assets.Scripts.Tracker
{
    public enum EventTypeEnum
    {
        GAMEPLAY_EVENT,
        MENU_EVENT,
        ACHIVEMENTS_EVENT,
        MISCELLANEOUS_EVENT,
    }
}
