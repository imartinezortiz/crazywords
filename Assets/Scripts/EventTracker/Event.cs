﻿using Assets.Scripts.Tracker.EventAttributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;

namespace Assets.Scripts.Tracker
{
    [Serializable]
    public class Event
    {
        [JsonProperty(PropertyName = "actor")]
        public ActorEventAttribute actorAttribute;

        [JsonProperty(PropertyName = "verb")]
        public VerbEventAttribute verbAttribute;

        [JsonProperty(PropertyName = "object")]
        public ObjectEventAttribute objectAttribute;


        /*
         * result: {
         *      score:{
         *          sacled: float,
         *          raw: float,
         *          min: float,
         *          max: float
         *      },
         *      completion: bool,
         *      success: bool,
         *      duration: ISO8601 duration (P352.18S)
         *      extensions: Dictionary<string, object>
         * }
         */
        [JsonProperty(PropertyName = "result")]
        public Dictionary<string, object> resultAttribute = new Dictionary<string, object>();

        [JsonProperty(PropertyName = "context")]
        public ContextEventAttribute contextAttribute;

        public string timestamp;
    }
}
