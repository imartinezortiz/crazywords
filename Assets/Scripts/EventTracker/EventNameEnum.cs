﻿namespace Assets.Scripts.Tracker
{
    public enum EventNameEnum
    {
        START,
        TABLE_STATE,
        HAND_STATE,
        MOVE,
        GENERATE_CARD,
        IDLE,
        RETURN_FROM_IDLE,
        HELP,
        END
    }
}
