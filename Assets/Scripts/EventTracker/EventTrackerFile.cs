﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections;
using System.IO;

namespace Assets.Scripts.Tracker
{
    public class EventTrackerFile: IEventTracker
    {
        public IEnumerator TrackEvent(Event eventToTrack)
        {
            using (StreamWriter sw = File.AppendText("trackedEvents.txt"))
            {
                var settings = new JsonSerializerSettings();
                settings.Converters.Add(new StringEnumConverter());
                sw.Write(JsonConvert.SerializeObject(eventToTrack, settings) + ',');
            }
            yield return true;

        }
    }
}
